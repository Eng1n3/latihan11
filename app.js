const fs = require("fs");
const validator = require("validator");
const chalk = require("chalk");
//const readline = require("readline");

//const rl = readline.createInterface({
//  input: process.stdin,
//  output: process.stdout,
//});

const dirPath = "./data";
if (!fs.existsSync(dirPath)) {
  fs.mkdirSync(dirPath);
}

const dataDB = "./data/db.json";
if (!fs.existsSync(dataDB)) {
  fs.writeFileSync(dataDB, "[]", "utf-8");
}

//const pertanyaan = (kalimat) => {
//  return new Promise((resolve, reject) => {
//    rl.question(kalimat, (jawab) => {
//      resolve(jawab);
//    });
//  });
//};

const simpanContact = (nama, nomor, email) => {
  const contact = { nama, nomor, email };
  const fileDummy = fs.readFileSync("./data/db.json", "utf8");
  const contacts = JSON.parse(fileDummy);
  const duplikatNama = contacts.find(contact => contact.nama === nama );
  if (duplikatNama) {
    console.log(chalk.red.inverse.bold("Nama sudah digunakan"));
    return false;
  }
  if ( email ) {
    if (!validator.isEmail(email)) {
      console.log(chalk.red.inverse.bold("Email tidak valid"));
      return false;
    }
  }
  if ( nomor ) {
    if (!validator.isMobilePhone(nomor, "id-ID")) {
      console.log(chalk.red.inverse.bold("Nomor tidak valid"));
      return false;
    }
  }
  contacts.push(contact);
  fs.writeFileSync("./data/db.json", JSON.stringify(contacts));
  console.log(chalk.green.inverse.bold("Terimakasih sudah menginput data"));
};

// rl.question("Masukkan nama anda: ", (nama) => {
//   rl.question("Masukkan angka: ", (angka) => {
//     const contact = { nama, angka };
//     const fileDummy = fs.readFileSync("db.json", "utf8");
//     const contacts = JSON.parse(fileDummy);
//     contacts.push(contact);
//     fs.writeFileSync("db.json", JSON.stringify(contacts));
//     rl.close();
//   });
// });

module.exports = { simpanContact };

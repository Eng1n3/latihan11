const yargs = require("yargs");
const {simpanContact} = require("./app");

yargs.command({
	command: "add",
	describe: "Menambahkan contact baru",
	builder: {
		nama: {
			describe: "Nama Lengkap",
			demandOption: true,
			type: "string"
		},
		nomor: {
			describe: "Nomor HP",
			demandOption: true,
			type: "string",
		},
		email: {
			describe: "Email",
			demandOption: false,
			type: "string"
		},
	},
	handler(argv) {
		simpanContact(argv.nama, argv.nomor, argv.email)
	}
})

yargs.parse()
